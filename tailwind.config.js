import flowbitePlugin from 'flowbite/plugin';

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}', './node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      backgroundImage: {
        main: "url('/main_background.png')",
        binary: "url('/binary_bg.png')",
      },
      colors: {},
      fontSize: {
        xxl: ['16rem', '1'],
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
        'nunito-sans': ['"Nunito Sans"'],
      },
    },
  },
  safelist: [
    'hidden',
    {
      // width/height/padding/margin classes because some are used dynamically (ex, <Spinner.svelte>)
      pattern: /(w|h|p[se]?|m[se]?)-([0-9]+)/,
    },
  ],
  plugins: [flowbitePlugin],
  darkMode: 'class',
};
