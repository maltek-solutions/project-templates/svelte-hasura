/// <references types="houdini-svelte">
import { resolvers } from 'graphql-scalars';

/** @type {import('houdini').ConfigFile} */
const config = {
  plugins: {
    'houdini-svelte': {},
  },
  features: {
    imperativeCache: true,
  },
  defaultFragmentMasking: 'disable',
  scalars: {
    uuid: {
      // the corresponding typescript type
      type: resolvers['UUID'].name,
      // turn the api's response into that type
      unmarshal(val) {
        return resolvers['UUID'].parseValue(val);
      },
      // turn the value into something the API can use
      marshal(val) {
        return resolvers['UUID'].serialize(val);
      },
    },
    // the name of the scalar we are configuring
    timestamptz: {
      // the corresponding typescript type
      type: resolvers['DateTime'].name,
      // turn the api's response into that type
      unmarshal(val) {
        return resolvers['DateTime'].parseValue(val);
      },
      // turn the value into something the API can use
      marshal(val) {
        return resolvers['DateTime'].serialize(val);
      },
    },
  },
};

export default config;
