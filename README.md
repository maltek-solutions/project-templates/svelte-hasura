# Sveltekit and Hasura Template

## Sveltekit User Interface

> **NOTE:** The following commands should be run from the `ui/` directory of the repo.

1. Install the correct version of node. This project uses node 18.17.1 to match Cloudflare Workers.

    - The recommended installation method utilizes
      [NVM](https://github.com/nvm-sh/nvm?tab=readme-ov-file#installing-and-updating).
      After installing, run `nvm use` to install and use the correct Node version.

2. Install dependencies

    ```shell
    npm install
    ```

3. Create an `.env` file with the following configuration:

    ```dotenv
    # graphql endpoint
    PUBLIC_GQL_PUBLIC_URI=http://localhost:8080/v1/graphql

    # oidc server settings
    PUBLIC_OIDC_AUTHORITY=
    PUBLIC_OIDC_CLIENT_ID=
    #PUBLIC_OIDC_SCOPE="openid profile email"
    ```

4. Run the development server

    ```shell
    npm run dev
    ```

## Hasura

### Running graphql

1. Start the hasura server

    ```shell
    docker compose up -d
    ```

### Console

Run the following to open the hasura console

```shell
cd hasura
hasura console
```

### Export Metadata and migrations

1. Export the hasura metadata

    ```shell
    hasura metadata export --admin-secret $HASURA_ADMIN_SECRET --project hasura
    ```

2. Export the hasura migrations

    ```shell
    hasura migrate create "migration_name" --admin-secret $HASURA_ADMIN_SECRET \
            --project hasura --from-server --database-name public --schema public
    ```

3. Commit the changes to the repo, and follow normal procedures for merging to the default branch.

### Apply Metadata and Migrations to Production

Run the following to commit the repo changes to production.

```shell
hasura metadata apply --admin-secret $HASURA_ADMIN_SECRET --project hasura
hasura migrate apply --admin-secret $HASURA_ADMIN_SECRET --project hasura --database-name default
```
