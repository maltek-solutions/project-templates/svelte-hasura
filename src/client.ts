import { HoudiniClient, ClientPlugin } from '$houdini';
import { subscription } from '$houdini/plugins';
import { oidc } from '$lib/stores';
import { createClient } from 'graphql-ws';
import { get } from 'svelte/store';
import Env from '$lib/env';

const oidcPlugin: ClientPlugin = () => {
  return {
    async beforeNetwork(ctx, { next }) {
      let token;
      try {
        token = get(oidc)?.token;

        // Enable to print JWT
        // console.log(token)
      } catch (e) {
        console.warn(`error getting token: ${e}`);
        return next(ctx);
      }
      if (token == null) {
        console.debug('no token available, not adding to headers');
        return next(ctx);
      }

      // Set session token for use in other plugins (like graphql-ws)
      if (!ctx.session) {
        console.debug("unsure why session is null, can't set session token");
      } else {
        ctx.session.token = token;
      }

      if (ctx.fetchParams == null) {
        console.debug("unsure why fetchParams is null, can't set Authorization header");
        return next(ctx);
      }

      const headers: Record<string, string> = {
        Authorization: `Bearer ${token}`,
      };

      if (ctx.metadata?.role) {
        headers['x-hasura-role'] = ctx.metadata.role;
      }

      ctx.fetchParams.headers = headers;

      // Enable to print queries and inputs
      // console.log(ctx.text)
      // console.log(ctx.variables)
      next(ctx);
    },
  };
};

export default new HoudiniClient({
  url: Env.GqlPublicUri,
  plugins: [
    oidcPlugin,
    subscription((ctx) =>
      createClient({
        url: Env.GqlPublicUri.replace('http', 'ws'),
        connectionParams: () => {
          console.debug('setting connectionParams for subscription', ctx);
          return {
            headers: ctx.fetchParams?.headers,
          };
        },
      })
    ),
  ],
});
