// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    // interface PageData {}
    // interface PageState {}
    // interface Platform {}

    // Recommended from https://houdinigraphql.com/guides/typescript#global-types
    interface Session {
      token: string | null;
    }

    interface Metadata {
      // used by houdini client.ts
      role: string | undefined;
    }
  }
}

export {};
