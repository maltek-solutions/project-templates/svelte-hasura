import * as appEnv from '$env/static/public';

export type AppEnvironment = {
  IntercomWarning?: string;
  IntercomError?: string;

  GqlPublicUri: string;

  OidcAuthority: string;
  OidcClientId: string;
  OidcScope: string;
};

const ENV: AppEnvironment = {
  // @ts-ignore These values may not exist in CI for builds
  IntercomWarning: appEnv.PUBLIC_INTERCOM_WARNING,
  // @ts-ignore These values may not exist in CI for builds
  IntercomError: appEnv.PUBLIC_INTERCOM_ERROR,
  // @ts-ignore These values may not exist in CI for builds
  GqlPublicUri: appEnv.PUBLIC_GQL_PUBLIC_URI as string,
  // @ts-ignore These values may not exist in CI for builds
  OidcAuthority: appEnv.PUBLIC_OIDC_AUTHORITY as string,
  // @ts-ignore These values may not exist in CI for builds
  OidcClientId: appEnv.PUBLIC_OIDC_CLIENT_ID as string,
  // @ts-ignore These values may not exist in CI for builds
  OidcScope: (appEnv.PUBLIC_OIDC_SCOPE as string) || 'openid profile email',
};

declare global {
  interface Window {
    AppConfig: AppEnvironment;
  }
}

console.debug('ENV: ', ENV);
export default ENV;
