import { type Writable, writable } from 'svelte/store';
import { type OidcManager } from '$lib/oidc';
import { BasicUserFields$data } from '$houdini';

export type UISettings = {
  flags: {
    [key: string]: boolean;
  };
};

export const oidc: Writable<OidcManager> = writable();
export const user: Writable<BasicUserFields$data> = writable();

export const settings: Writable<UISettings> = writable({
  // load initial settings from env or localStorage here
  flags: {},
});
