import Env from '$lib/env';
import { oidc, user as userStore } from '$lib/stores';
import {
  Log,
  type SigninSilentArgs,
  type SignoutSilentArgs,
  User as DefaultUser,
  UserManager,
  type UserProfile as DefaultUserProfile,
} from 'oidc-client-ts';
import { goto } from '$app/navigation';
import { get, type Writable } from 'svelte/store';
import { type SubscriptionStore, SubUserByPkStore } from '$houdini';

export type NullableEventRole = GqlRole | null | undefined;
export enum GqlRole {
  SuperAdmin = 'admin',
  Anonymous = 'anonymous',
}

export type UserProfile = DefaultUserProfile & {
  // These fields are known to be populated from Keycloak
  given_name: string;
  family_name: string;
  name: string;
  email: string;
  email_verified: boolean;
  preferred_username: string;
  // standard hasura claims
  'https://hasura.io/jwt/claims'?: {
    'x-hasura-default-role': GqlRole;
    'x-hasura-allowed-roles': GqlRole[];
    'x-hasura-user-email': string;
    'x-hasura-user-id': string;
  };
  exp: number;
};

export type User = DefaultUser & { profile: UserProfile };

/**
 * Class for interfacing with Keycloak.
 * !!WARNING!! Should not be used directly except for initialization! Use the keycloak store.
 * !!WARNING!! Most Errors in this class will cause an infinite reload loop. WE MUST HANDLE ALL ERRORS
 */
export class OidcManager extends UserManager {
  // store current user as UserManager.getUser() is async
  currentUser: User | null = null;
  // todo update with your user type from gql and remove this eslint-disable
  /* eslint-disable  @typescript-eslint/no-explicit-any */
  _gqlUserSubscription: SubscriptionStore<any, any> | null = null;

  /*
   * Convenience Getters
   */
  public get authenticated(): boolean {
    console.log('authenticated', this.currentUser);
    return !!this.currentUser;
  }

  public get token(): string | undefined {
    return this.currentUser?.access_token;
  }

  public get tokenParsed(): UserProfile | undefined {
    return this.currentUser?.profile;
  }

  public get userId(): string | undefined {
    return this.currentUser?.profile.sub;
  }

  /**
   * Try to retrieve user email from a known field
   */
  public get userEmail(): string | undefined {
    return this.currentUser ? this._extractUserEmail(this.currentUser.profile as UserProfile) : undefined;
  }

  private _extractUserEmail(profile: UserProfile): string | undefined {
    return profile['https://hasura.io/jwt/claims']?.['x-hasura-user-email'] || profile.email;
  }

  public get hasuraRoles(): GqlRole[] | null {
    return this.currentUser?.profile['https://hasura.io/jwt/claims']?.['x-hasura-allowed-roles'] || null;
  }

  public get hasuraDefaultRole(): string | undefined {
    return this.currentUser?.profile['https://hasura.io/jwt/claims']?.['x-hasura-default-role'];
  }

  async logout() {
    // Get to an unauthenticated page first, then logout and clear the stores
    await goto('/');
    await this.signoutSilent();
  }

  async init() {
    /**
     * Init should be called once, immediately after the client is created.
     * It should be called outside the constructor, so it can be awaited,
     * otherwise race conditions may occur. Ex, between init.signinCallback and UserManager.signinSilent
     */
    let user = null;

    try {
      if (window.location.search.includes('state=')) {
        user = ((await this.signinCallback(window.location.search)) as User | void) || null;
        // clear search from URL - we intentionally call this after signinCallback
        // so we have the original url in the browser if signinCallback throws an error
        // console.warn('resetting search')
        // window.location.search = '';
        const url = new URL(window.location.href);
        url.search = '';
        history.replaceState(null, '', url.toString());
      }
    } catch (e) {
      console.error('init failed');
      console.error(e);
    } finally {
      await this.updateStores(user);
    }
  }

  async signinSilent(args?: SigninSilentArgs): Promise<User | null> {
    const user = (await super.signinSilent(args)) as User | null;
    await this.updateStores(user);
    return user as User;
  }

  async signoutSilent(args?: SignoutSilentArgs): Promise<void> {
    await this.updateStores(null);
    await super.signoutSilent(args);
  }

  async updateStores(user: User | null) {
    // This could cause pain the future. When a token is refreshed,
    // an Access Token is guaranteed but an ID Token is not.
    // As far as we can tell, user.profile is a representation of the ID token and not the Access Token.
    // At time of writing, this is not an issue as Keycloak always populates the ID Token
    // WITH all information needed for UserProfile such as hasura roles,
    // but a configuration change could break this in the future.
    // update our Svelte store

    //  if we don't currently have a user email to watch, or the email has changed, update the GQL subscription
    const updateSubscription: boolean =
      !this.userEmail || (!!user?.profile && this._extractUserEmail(user.profile as UserProfile) !== this.userEmail);

    // unsubscribe from current email, if subscription exists
    if (updateSubscription && this._gqlUserSubscription) {
      await this._gqlUserSubscription.unlisten();
      console.debug('unsubscribed from user', this.userEmail, this._gqlUserSubscription);
      this._gqlUserSubscription = null;
    }

    this.currentUser = (user as User) || null;
    oidc.set(this);

    // subscribe to gql, watching new email for user information
    // todo update your query for the actual user here
    if (updateSubscription && this.userEmail) {
      console.debug('subscribing to user', this.userEmail);
      this._gqlUserSubscription = new SubUserByPkStore();
      await this._gqlUserSubscription.listen({ email: this.userEmail });
      this._gqlUserSubscription.subscribe((result) => {
        if (result.errors?.length) console.warn('errors fetching gql user from OIDC email', result.errors);

        if (result.data?.users_by_pk) userStore.set(result.data.users_by_pk);
      });
    }
  }

  isRole(role: GqlRole, allowSuper = true): boolean {
    if (allowSuper && this.hasuraRoles && this.hasuraRoles.indexOf(GqlRole.SuperAdmin) !== -1) return true;
    return this.hasuraRoles ? this.hasuraRoles.indexOf(role) !== -1 : false;
  }

  getFirstRole(roles: GqlRole[], allowSuper = true): GqlRole | null {
    if (allowSuper) roles = [GqlRole.SuperAdmin, ...roles];
    for (const role of roles) {
      if (this.isRole(role)) return role;
    }
    return null;
  }
}

export type CreateClientArgs = {
  trySilentAuth?: boolean;
  oidcLogLevel?: Log;
  statusStore?: Writable<string>;
};

export async function createClient(args?: CreateClientArgs): Promise<OidcManager> {
  args?.statusStore?.set('[createClient] start');
  if (get(oidc)) {
    console.debug('oidcClient already exists'); // this happens in the dev server during hot-reloads
    return get(oidc);
  }
  const client = new OidcManager({
    authority: Env.OidcAuthority,
    client_id: Env.OidcClientId,
    // setting redirect_uri to window.location isn't perfect because it's the url when the page first loaded,
    // but this is a SPA and the url changes as the user navigates around. As of time of writing, there
    // are no routes that a user can click requiring authentication except the login button, so this is fine.
    // We may need to find some was to dynamically update this value in the future.
    // We use origin + pathname to discard the hash, which is used by oidc-client-ts.
    redirect_uri: window.location.origin,
    silent_redirect_uri: window.origin + '/ssoRedirect',
    silentRequestTimeoutInSeconds: 4,
    response_type: 'code',
    scope: Env.OidcScope,

    response_mode: 'query',
    filterProtocolClaims: true,
  });
  await client.init(); // perform init outside the constructor so it can be awaited
  oidc.set(client);

  if (args?.oidcLogLevel) {
    args?.statusStore?.set('[createClient] setup logging');
    Log.setLogger({
      debug: console.debug,
      info: console.info,
      warn: console.warn,
      error: console.error,
    });
    Log.setLevel(args.oidcLogLevel);
  }

  if (args?.trySilentAuth && !client.currentUser) {
    args?.statusStore?.set('[createClient] try silent auth');
    try {
      await client.signinSilent();
    } catch (err) {
      console.error(err);
    }
  }

  return client;
}
